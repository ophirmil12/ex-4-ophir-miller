from http.server import BaseHTTPRequestHandler, HTTPServer
import logging


def split_msg(parameters):
    parameters = parameters.split("&")
    new_parameters_list = []
    for parameter in parameters:
        new_parameters_list.append(parameter.split("="))
    return new_parameters_list


def command_checker(function_to_do, parameters):
    # the try except - [bonus 2]
    try:
        # sum function
        if function_to_do == "/average":
            num_sum = 0
            # can sum multiple parameters - [bonus 3]
            for num in parameters:
                num_sum += int(num[1])
            return str(num_sum / len(parameters))
        # average function [bonus 1]
        if function_to_do == "/sum":
            num_sum = 0
            # can sum multiple parameters - [bonus 3]
            for num in parameters:
                num_sum += int(num[1])
            return str(num_sum)
    except:
        return "ValueError.     Try other parameters."


class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        print("do_GET")
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        if self.path != "/favicon.ico":
            function_to_do, parameters = self.path.split("?")
            parameters = split_msg(parameters)
            text_to_return = "The result of " + function_to_do[1:] + " of this numbers: " + str(parameters) + " is " + command_checker(function_to_do, parameters)
            html_to_return = """
            <html>
            <head>
            <title>Ophir Miller</title>
            </head>
            <body>
            <h1 style="color:blue">Welcome to Miller`s page!</h1>
            <p style="color:green">{}</p>
            </body>
            </html>
            """.format(text_to_return)
            self.wfile.write(html_to_return.encode('utf-8'))

    def do_POST(self):
        print("do_POST")
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        print(post_data)
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                     str(self.path), str(self.headers), post_data.decode('utf-8'))

        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        print(self.path)


def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
